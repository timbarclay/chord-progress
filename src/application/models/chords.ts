import { toRoman } from 'roman-numerals'

export class Chord {
  name: string
  chordSymbolSuffix: string
  universalKeySuffix: string
  isMajor: boolean
  triadIntervals: number[]
  
  constructor (name: string, chordSymbolSuffix: string, universalKeySuffix: string, isMajor: boolean, triadIntervals: number[]) {
    this.name = name
    this.chordSymbolSuffix = chordSymbolSuffix
    this.universalKeySuffix = universalKeySuffix
    this.isMajor = isMajor
    this.triadIntervals = triadIntervals
  }

  asChordSymbol (note: string) {
    return `${note}${this.chordSymbolSuffix}`
  }

  asChordNumeral (degree: number) {
    const numeral = toRoman(degree)
    const cased = this.isMajor ? numeral.toUpperCase() : numeral.toLowerCase()
    return `${cased}${this.universalKeySuffix}`
  }
}

export const majorChord = new Chord('major', '', '', true, [1, 5, 8])
export const minorChord = new Chord('minor', 'm', '', false, [1, 4, 8])
export const diminishedChord = new Chord('diminished', '<sup>o</sup>', '<sup>o</sup>', false, [1, 4, 7])
export const augmentedChord = new Chord('augmented', '<sup>+</sup>', '<sup>+</sup>', true, [1, 5, 9])
export const major7th = new Chord('major 7th', '<sup>maj7</sup>', 'maj<sup>7</sup>', true, [1, 5, 8, 12])
export const minor7th = new Chord('minor 7th', 'm<sup>7</sup>', '<sup>7</sup>', false, [1, 4, 8, 11])
export const dominant7th = new Chord('dominant 7th', '<sup>7</sup>', '<sup>7</sup>', true, [1, 5, 8, 11])
export const minor7thFlat5 = new Chord('minor 7th flat 5', '<sup>ø</sup>', '<sup>ø</sup>', false, [1, 4, 7, 11])
export const minorMajor7 = new Chord('minor major 7', 'm<sup>M7</sup>', 'maj<sup>7</sup>', false, [1, 4, 8, 12])
export const major7thSharp5 = new Chord('major 7th sharp 5', '+<sup>M7</sup>', '+<sup>M7</sup>', true, [1, 5, 9, 12])
export const diminished7th = new Chord('diminished 7th', '<sup>o7</sup>', '<sup>o7</sup>', false, [1, 4, 7, 10])
