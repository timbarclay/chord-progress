import { Scale } from './scales'
import * as TonalScale from "tonal-scale"

export class KeyScaleChord {
  chordSymbol: string
  chordNumeral: string
  constructor (chordSymbol: string, chordNumeral: string) {
    this.chordSymbol = chordSymbol
    this.chordNumeral = chordNumeral
  }
}

export class KeyScaleDegree {
  note: string
  chords: KeyScaleChord[]
  constructor (note: string, chords: KeyScaleChord[]) {
    this.note = note
    this.chords = chords
  }
}

export class ScaleInKey {
  key: string
  scale: Scale
  
  constructor (key: string, scale: Scale) {
    this.key = key
    this.scale = scale
  }

  get degrees (): KeyScaleDegree[] {
    return TonalScale.notes(`${this.key} ${this.scale.tonalName}`).map((note, i) => {
      const scaleDegree = this.scale.degrees[i]
      return new KeyScaleDegree(note, scaleDegree.chords.map(d => new KeyScaleChord(d.asChordSymbol(note), d.asChordNumeral(i + 1))))
    })
  }

  get scaleNotes (): string[] {
    return TonalScale.notes(`${this.key} ${this.scale.tonalName}`)
  }

  get scaleChords () {
    return this.scaleNotes.map((n, i) => {
      return this.scale.degrees[i].chords[0].asChordSymbol(n)
    })
  }

  get name () {
    return `${this.key} ${this.scale.name}`
  }
}