import {
  Chord,
  majorChord,
  minorChord,
  diminishedChord,
  augmentedChord,
  major7th,
  minor7th,
  dominant7th,
  minor7thFlat5,
  minorMajor7,
  major7thSharp5,
  diminished7th
} from './chords'

export class ScaleDegree {
  degree: number
  intervalFromRoot: number
  chords: Chord[]
  constructor (degree: number, intervalFromRoot: number, chords: Chord[]) {
    this.degree = degree
    this.intervalFromRoot = intervalFromRoot
    this.chords = chords
  }
}

export class Scale {
  name: string
  tonalName: String
  isMajor: boolean
  degrees: ScaleDegree[]
  
  constructor (name: string, tonalName: string, isMajor: boolean, degrees: ScaleDegree[]) {
    this.name = name
    this.tonalName = tonalName
    this.isMajor = isMajor
    this.degrees = degrees
  }
}

export const majorScale = new Scale('major', 'major', true, [
  new ScaleDegree(1, 1, [majorChord, major7th]),
  new ScaleDegree(2, 3, [minorChord, minor7th]),
  new ScaleDegree(3, 5, [minorChord, minor7th]),
  new ScaleDegree(4, 6, [majorChord, major7th]),
  new ScaleDegree(5, 8, [majorChord, dominant7th]),
  new ScaleDegree(6, 10, [minorChord, minor7th]),
  new ScaleDegree(7, 12, [diminishedChord, minor7thFlat5])
])

export const naturalMinorScale = new Scale('natural minor', 'aeolian', false, [
  new ScaleDegree(1, 1, [minorChord, minor7th]),
  new ScaleDegree(2, 3, [diminishedChord, minor7thFlat5]),
  new ScaleDegree(3, 4, [majorChord, major7th]),
  new ScaleDegree(4, 6, [minorChord, minor7th]),
  new ScaleDegree(5, 8, [minorChord, minor7th]),
  new ScaleDegree(6, 9, [majorChord, major7th]),
  new ScaleDegree(7, 11, [majorChord, dominant7th])
])

export const harmonicMinorScale = new Scale('harmonic minor', 'harmonic minor', false, [
  new ScaleDegree(1, 1, [minorChord, minorMajor7]),
  new ScaleDegree(2, 3, [diminishedChord, minor7thFlat5]),
  new ScaleDegree(3, 4, [augmentedChord, major7thSharp5]),
  new ScaleDegree(4, 6, [minorChord, minor7th]),
  new ScaleDegree(5, 8, [majorChord, dominant7th]),
  new ScaleDegree(6, 9, [majorChord, major7th]),
  new ScaleDegree(7, 12, [diminishedChord, diminished7th])
])

export const scales = [majorScale, naturalMinorScale, harmonicMinorScale]